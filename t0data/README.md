These configurations are used to cross-check Rucio subscriptions of data
export during data taking. They are run in a dry-run mode and send emails if
they find something to replicate (i.e. Rucio did not properly replicate some
data).